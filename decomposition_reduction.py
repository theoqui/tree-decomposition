from sage.graphs.graph import Graph
from sage.sets.set import Set

def is_valid_tree_decomposition(G, T):
    """
    Check whether `T` is a valid tree-decomposition for `G`.
    """
    # 1. The union of the bags equals V
    if set(G) != set(chain(*T)):
        print("Not all vertices are covered")
        return False

    # 2. Each edge of G is contained in a bag
    vertex_to_bags = {u: set() for u in G}
    for Xi in T:
        for u in Xi:
            vertex_to_bags[u].add(Xi)

    for u, v in G.edge_iterator(labels=False):
        if not any(v in Xi for Xi in vertex_to_bags[u]):
            print("Not all edges are covered")
            return False

    # 3. The bags containing a vertex v form a connected subset of T
    for X in vertex_to_bags.values():
        D = DisjointSet(X)
        for Xi in X:
            for Xj in T.neighbor_iterator(Xi):
                if Xj in X:
                    D.union(Xi, Xj)
        if D.number_of_subsets() > 1:
            print("The subtree induces by a vertex is not connected")
            return False

    return True

def reduce_decomposition(G, H):
    """
    input : G a graph and H a tree-decomposition of G
    output : if possible, a decomposition of smaller length, H otherwise
    """

    # work on a copy of H to be able to roll back changes if needed
    H1 = H.copy()

    # first we have to simplify the decomposition
    done_merging = False
    while not done_merging:
        if len(H1.edges()) == 0:
            break
        for (u, v, w) in list(H1.edges()):
            done_merging = True
            if u.issuperset(v):
                if u in H1 and v in H1:
                    H1.merge_vertices([u, v])
                    done_merging = False
                else:
                    done_merging = False
            elif v.issuperset(u):
                if u in H1 and v in H1:
                    H1.merge_vertices([v, u])
                    done_merging = False
                else:
                    done_merging = False

    # count the number of bags an edge appears in
    edge_count = dict()

    # list the bags an edge appears in
    edge_bags = dict()
    for e in G.edges():
        edge_count[e] = 0
        edge_bags[e] = set()

    # find which edge(s) are in the most bags
    max_counted_edges = set()
    max_count = 0

    multiple_counted_edges = set()

    # for each bag in the decomposition, count the edges
    for bag in list(H1.vertices()):
        for (u, v, w) in G.subgraph(bag).edges():
            if (u, v, w) in edge_count:
                edge_count[(u, v, w)] += 1
                if edge_count[(u, v, w)] == 2:
                    multiple_counted_edges.add((u, v, w))
            else:
                edge_count[(v, u, w)] += 1
                if edge_count[(v, u, w)] == 2:
                    multiple_counted_edges.add((v, u, w))

            # if the edge (u,v) is not found, then it's been counted as (v, u)
            if (u,v ,w) in edge_bags:
                edge_bags[(u, v, w)].add(bag)
                if edge_count[(u, v, w)] > max_count:
                    max_counted_edges = set([(u, v, w)])
                    max_count = edge_count[(u, v, w)]
                elif edge_count[(u, v, w)] == max_count:
                    max_counted_edges.add((u, v, w))
            else:
                edge_bags[(v, u, w)].add(bag)
                if edge_count[(v, u, w)] > max_count:
                    max_counted_edges = set([(v, u, w)])
                    max_count = edge_count[(v, u, w)]
                elif edge_count[(v, u, w)] == max_count:
                    max_counted_edges.add((v, u, w))

    # remember which vertices have been removed from each bag
    removed_vertices = dict()
    for bag in H.vertices():
        removed_vertices[bag] = set()

    max_counted_edges = multiple_counted_edges

    for (u, v, w) in max_counted_edges:
        if (u, v, w) in edge_bags:
            bags_uv = edge_bags[(u, v, w)]
        else:
            bags_uv = edge_bags[(v, u, w)]

        for bag in bags_uv:
            updated_bag = bag.difference(removed_vertices[bag])
            # we only try to remove if the edge is counted more than once
            # which can change with iterations
            if (u, v, w) in edge_count and edge_count[(u, v, w)] == 1:
                continue
            elif (v, u, w) in edge_count and edge_count[(v, u, w)] == 1:
                continue

            removable = set()

            # we can delete u only if it is in at most one adjacent bag
            nb_neighboring_bags = 0
            for neighbor_bag in H1.neighbors(updated_bag):
                if u in neighbor_bag:
                    nb_neighboring_bags += 1

            if nb_neighboring_bags <= 1:
                # u can be removed from the bag if all the edges that have
                # u as an endpoint have been counted more than once
                can_remove_u = True
                for (x, y, z) in G.subgraph(updated_bag).edges_incident([u]):
                    if (x, y, z) in edge_count and edge_count[(x, y, z)] <= 1:
                        can_remove_u =  False
                        break
                    elif (y, x, z) in edge_count and edge_count[(y, x, z)] <= 1:
                        can_remove_u = False
                        break

                if can_remove_u:
                    removable.add(u)

            ### do the same for v
            nb_neighboring_bags = 0
            for neighbor_bag in H1.neighbors(updated_bag):
                if v in neighbor_bag:
                    nb_neighboring_bags += 1

            if nb_neighboring_bags <= 1:
                # v can be removed from the bag if all the edges that have
                # v as an endpoint have been counted more than once
                can_remove_v = True
                for (x, y, z) in G.subgraph(updated_bag).edges_incident([v]):
                    if (x, y, z) in edge_count and edge_count[(x, y, z)] <= 1:
                        can_remove_v =  False
                        break
                    elif (y, x, z) in edge_count and edge_count[(y, x, z)] <= 1:
                        can_remove_v = False
                        break

                if can_remove_v:
                    removable.add(v)

            # all the edges incident to the removed vertices are now in one bag less than before
            affected_edges = []
            for x in removable:
                affected_edges = affected_edges + G.subgraph(updated_bag).edges_incident([x])

            for (x, y, z) in affected_edges:
                if not ((x == u and y == v) or (x == v and y == u)):
                    if (x, y, z) in edge_count:
                        edge_count[(x, y, z)] -= 1
                    else:
                        edge_count[(y, x, z)] -= 1
            # we do it separately for (u,v) to avoid counting it twice
            if len(removable) > 0 and (u, v, w) in edge_count:
                edge_count[(u, v, w)] -= 1
            elif len(removable) > 0 and (v, u, w) in edge_count:
                edge_count[(v, u, w)] -= 1


            # change the bag to a new one without the removed vertices
            new_bag = updated_bag.difference(removable)
            removed_vertices[bag] = removed_vertices[bag].union(removable)
            old_bag_neighbors = H1.neighbors(updated_bag)
            if new_bag in old_bag_neighbors:
                for neigh in old_bag_neighbors:
                    if not neigh == new_bag:
                        print("new edge : {} - {}".format(new_bag, neigh))
                        H1.add_edge(new_bag, neigh)
            else:
                H1.delete_vertex(updated_bag)
                for neigh in old_bag_neighbors:
                    H1.add_edge(new_bag, neigh)

    # get rid of the cycles : we have to simplify the decomposition again
    done_merging = False
    while not done_merging:
        if len(H1.edges()) == 0:
            break
        for (u, v, w) in list(H1.edges()):
            done_merging = True
            if u.issuperset(v):
                if u in H1 and v in H1:
                    H1.merge_vertices([u, v])
                    done_merging = False
                else:
                    done_merging = False
            elif v.issuperset(u):
                if u in H1 and v in H1:
                    H1.merge_vertices([v, u])
                    done_merging = False
                else:
                    done_merging = False

    return H1
