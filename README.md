# Algorithmes

## Package treedecomposition

### Contenu

* Bfs-Layering
* Uncle-trees
* Lex-M
* Disk-tree (avec recherche linéaire du paramètre k)
* Disk-tree (avec recherche dichotomique du paramètre k) 
* Interface permettant de calculer rapidement les distances dans un graphe (distance\_interface.py) (fournie par D. Coudert)
* Outils pour la décomposition (decomp\_tools)
⋅* Méthode pour vérifier qu'une tree-decomposition est valide

### Utilisation

Il suffit de copier le dossier treedecomposition dans le répertoire de travail, d'où Sage est lancé. Il peut ensuite être importé comme n'importe quel autre package, par exemple :
```
import treedecomposition
G = Some\_graph()
H = treedecomposition.bfs\_layering\_decomposition(G)
```

```
from treedecomposition import lex\_m\_decomposition as lexm 
G = Some\_graph()
H = lexm(G)
```

## Paramètres
Chaque algorithme peut prendre un booléen _check_ en entrée, vrai par défaut, qui permet de faire quelques vérifications  au début et à la fin de l'exécution de l'algorithme, en particulier que le graphe retourné est bien une tree-decomposition du graphe en entrée. 

Tous peuvent être appelés avec uniquement le graphe à décomposer en argument, mais il est possible de préciser le sommet de départ (paramètre _source_) si on ne veut pas qu'il soit sélectionné "au hasard". 

---

# Répertoire tests

Contient les benchmarks effectués, en particulier le dossier _third\_tests_ contient tous les derniers fichiers de tests, graphes sous formes de listes d'arêtes, résultats, quelques graphiques, etc...

Le fichier _analysis.py_ contient des méthodes de traitement et représentation des données pour l'analyse, mais c'est du quick & dirty tout le long donc imbuvable et je n'en recommande pas l'utilisation pour quoi que ce soit.

---

# Autres fichiers (racine du projet)

## Decomposition\_reduction.py

Contient une méthode permettant, étant donné un graphe et une tree-decomposition de ce graphe, de réduire la décomposition ; c'est-à-dire d'obtenir une décomposition dont les feuilles ne sont pas inclues dans leur parent.

## disktree.py 

Méthode disk-tree pour la décomposition nécessitant de donner la valeur du paramètre k en argument, des petits bugs ont été résolus sur les versions par dichotomie et recherche linéaire après, il est donc possible que cette version fonctionne mal.

## lowerbounds.py 

Code pour calculer un plus long cycle isométrique, tiré d'un article de (il me semble) Lokshtanov. Peu commenté (désolé), et extrêmement lent (ce qui n'est pas surprenant au vu de la difficulté du problème).

## seriesparallel.py 

Génération de graphes série-parallèle âléatoires de tailles choisies. Peut probablement être amélioré pour obtenir des graphes plus divers.

## src\_treelength.py 

Méthode exacte pour calculer la treelength d'un graphe (fournie par D. Coudert).

## testing\_machine.py

Classe _Tester_ implémentant des méthodes pour automatiser (en grande partie) les benchmarks. Quelques unes seront explicitées ci-après.

### compute\_length(G, H, width)

Méthode pour calculer la length d'une tree-decomposition. Deux algorithmes peuvent être utilisés en fonction de la width de la décomposition. En effet, quand la width est trop grande une technique est bien plus lente que l'autre. Un seuil de 300 a été choisi expérimentalement, il peut être ajusté au besoin (une simple modification ligne 80).

### measure(G, decomp\_method, res, timeout, reduce=False, lb\_length=None)

Essaye de décomposer le graphe **G** en entrée avec la fonction **decomp\_method**. **res** est un objet Result (défini dans le fichier) dans lequel les résultats obtenus sont stockés (à cause de difficultés à gérer le timeout). **timeout** précise la durée du timeout en secondes, le paramètre **reduce** permet de choisir si la décomposition doit être réduite (avec la méthode présentée plus haut) ou pas, et enfin **lb\_length** est une borne inférieure sur la treelength du graphe à décomposer.

Un timeout de 0s signifie qu'il n'y a pas de timeout.

### connected(G)

Rend un graphe **G** connexe s'il ne l'est pas. Un sommet est choisi dans la "première" composante connexe et relié à un sommet de chaque autre composante.

### read\_graph(filename)

Charge le graphe contenu sous forme de liste d'arêtes dans le fichier **filename** sous forme d'un objet Graph.

### start\_testing(min\_size, max\_size, step, decomp\_method, quantity, dir, timeout, resume, use\_bounds)

Après avoir généré des graphes dans un répertoire _dir/files_, chaque fichier contenant un seul graphe avec un nom de fichier du type "\[taille\]-i.txt" (ex: _10-0.txt_). Des tests seront alors lancés sur les graphes de tailles comprises dans \[**min\_size** ; **max\_size**\], par pas de **step** et avec **quantity** graphes par taille. 

Le booléen **resume** permet de dire s'il s'agit de la reprise de tests déjà entamés et interrompus. **use\_bounds** est utilisé sur disktree pour savoir si on veut utiliser ou non les bornes sur la valeur de la treelength.

Les résultats sont rassemblés dans un répertoire _Results_ sous forme d'un fichier csv.