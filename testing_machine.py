# -*- coding: utf-8 -*-

from sage.graphs.graph import Graph
from uncleTrees import uncle_tree_decomposition
from bfslayering import bfs_layering_decomposition
from disktree_dichotomy import disk_tree_decomposition as disk_tree_decomposition_d
from disktree_increment import disk_tree_decomposition as disk_tree_decomposition_i
from lex_m import lex_m_decomposition
from sage.graphs.generators.random import RandomGNP, RandomBarabasiAlbert, RandomTriangulation, RandomChordalGraph
from sage.graphs.generators.basic import Grid2dGraph
from lowerbounds import *
import signal, random, time
from decomposition_reduction import reduce_decomposition
from seriesparallel import random_sp
import os
import math
import distance_interface

class TimeoutException(Exception):
    pass

def handler(signum, frame):
    raise TimeoutException("Execution timed out")

signal.signal(signal.SIGALRM, handler)

class Result:
    name = ""
    time = -1.0
    width = -1.0
    length = -1.0
    timeout = True
    reduced_length = -1.0

class Tester:
    names = dict()
    gnpProb = 0.72


    def __init__(self):
        self.names[uncle_tree_decomposition] = "uncle-trees"
        self.names[bfs_layering_decomposition] = "bfs layering"
        self.names[disk_tree_decomposition_i] = "disk-tree (incremental)"
        self.names[disk_tree_decomposition_d] = "disk-tree (dichotomy)"
        self.names[lex_m_decomposition] = "lexM"


    def mean(self, data):
        sum = 0
        for k in data:
            sum += k
        return sum / (len(data))


    def compute_width(self, H):
        """
        compute the width of a tree-decomposition H
        """
        width = 0
        # compute the width of each bag
        for v in H.vertices():
            width = max(width, len(v))
        return width + 1


    def compute_length(self, G, H, width=300):
        """"
        compute the length of a tree-decomposition H of a graph G
        """
        class Couple:
            def __init__(self, u, v, dist):
                self.u = u
                self.v = v
                self.dist = dist

            def __lt__(self, other):
                return self.dist > other.dist
            def __le__(self, other):
                return self.dist >= other.dist
        if width > 300:
            dist = distance_interface.Distances(G)
            couples = []

            # compute V(G)^2
            for u in G.vertices():
                for v in G.vertices():
                    if not u == v:
                        couples.append(Couple(u, v, dist[u, v]))

            # sort the couples in descending order
            couples = sorted(couples)

            # find the first couple with both vertices in the same bag
            for i in range(len(couples)):
                for bag in H.vertices():
                    if couples[i].u in bag and couples[i].v in bag:
                        return couples[i].dist
        else:
            length = 0
            dist = distance_interface.Distances(G)
            for bag in H.vertices():
                for u in bag:
                    for v in bag:
                        if not u == v:
                            length = max(length, dist[u, v])
            return length



    def measure(self, G, decomp_method, res, timeout, reduce=False, lb_length=None):
        """
        Tries to compute the decomposition of G with function decomp_method
        and write the computation time, width and length of the decomposition into res
        """
        res.name = self.names[decomp_method]
        success = False
        try:
            signal.alarm(timeout)

            start = time.time()
            if lb_length == None or lb_length == 0 or lb_length == 1:
                success, H = decomp_method(G)
            else:
                success, H = decomp_method(G, lower=lb_length, upper=3*lb_length) # divide the bound by 2 to get the radius of the bags
            end = time.time()

            signal.alarm(timeout) # cancel the previous alarm and give some time to compute the length and width
            res.timeout = False
            if success:
                res.time = end-start
                res.width = self.compute_width(H)
                res.length = self.compute_length(G, H, width=res.width)
                if reduce:
                    res.reduced_length = self.compute_length(G, reduce_decomposition(G, H))
            else:
                res.time = 0
                res.width = 0
                res.length = 0
            signal.alarm(0)
            return success, res
        except TimeoutException:
            print("Timeout")
            return success, res
        except Exception as e:
            print(e)
            return False, res



    def generate_cycle(self, size):
        G = Graph()
        G.add_cycle([i for i in range(size)])
        return G


    def generate_all(self, min_size, max_size, step, quantity, dir):
        """
        Generate the directories with test files for all the generation methods
        """
        paths = ["barabasi/A/", "barabasi/B/", "barabasi/C/", "GNP/A/", "GNP/B/", \
        "chordal/", "triangulation/", "cycle/", "seriesparallel/", "grids/", "lenghty/"]
        methods = [\
        lambda x: (self.connected(RandomBarabasiAlbert(x, 2)), 0),\
        lambda x: (self.connected(RandomBarabasiAlbert(x, int(math.log(x)))), 0),\
        lambda x: (self.connected(RandomBarabasiAlbert(x, int(math.sqrt(x)))), 0),\
        lambda x: (self.connected(RandomGNP(x, math.log(x)/x)), 0),\
        lambda x: (self.connected(RandomGNP(x, 1/x)), 0),\
        lambda x: (self.connected(RandomChordalGraph(x)), 1), \
        lambda x: (self.connected(RandomTriangulation(x)), 0),\
        lambda x: (self.generate_cycle(x), int(x/3)),\
        lambda x: (self.connected(random_sp(x)), 0),\
        lambda x: self.generate_grid(x, dim=True),\
        lambda x: self.generate_graph_length(x, random.randint(int(x / 4), int(x / 3)), lb=True)\
        ]
        for str in paths:
            os.makedirs(dir+"/"+str+"files/")

        for i in range(len(paths)):
            self.generate_family(min_size, max_size, step, quantity, dir+"/"+paths[i]+"files/", methods[i])


    def generate_family(self, min_size, max_size, step, quantity, dir, function):
        for i in range(min_size, max_size, step):
            for j in range(quantity):
                G, length_lb = function(i)
                f_out = open(dir+str(i)+"-"+str(j)+".txt", mode='w')
                f_out.writelines("lb,{},{}\n".format(0, length_lb))
                for e in G.edges():
                    f_out.writelines(str(e[0])+","+str(e[1])+"\n")
                f_out.close()


    def generate_files(self, min_size, max_size, step, quantity, dir):
        """
        generates connected graphs with vertex set size in range(min_size, max_size, step)
        quantity is the number of graphs of each size to produce
        the files are saved in the dir directory
        the generation method is the one specified in self.generate_graph
        filename : dir/size-num.txt
        """
        methods = dict()
        methods["Chordal"] = set()
        methods["Planar"] = set()
        methods["Clique"] = set()
        methods["Tree"] = set()
        for i in range(min_size, max_size, step):
            for j in range(quantity):
                G, method = self.generate_graph(i)
                #lb_tw, lb_tl = max_clique(G), longest_isometric_cycle(G) // 3
                lb_tw, lb_tl = 0, 0
                f_out = open(dir+"/files/"+str(i)+"-"+str(j)+".txt", mode='w')
                f_out.writelines("lb,{},{}\n".format(lb_tw, lb_tl))
                for e in G.edges():
                    f_out.writelines(str(e[0])+","+str(e[1])+"\n")
                f_out.close()

                if method in methods:
                    methods[method].add("{}-{}.txt".format(i, j))
                else:
                    methods[method] = set(["{}-{}.txt".format(i, j)])

                if G.is_chordal():
                    methods["Chordal"].add("{}-{}.txt".format(i, j))

                if G.is_planar():
                    methods["Planar"].add("{}-{}.txt".format(i, j))

                if G.is_clique():
                    methods["Clique"].add("{}-{}.txt".format(i, j))

                if G.is_tree():
                    methods["Tree"].add("{}-{}.txt".format(i, j))

        for method, graphs in methods.items():
            f_out = open(dir+"/info/"+"INFO-"+method+".txt", mode='w')
            for graph in graphs:
                f_out.writelines(graph+"\n")
            f_out.close()


    def connected(self, G):
        if not G.is_connected():
            components = G.connected_components()
            # add an edge between a vertex of the first connected component and a vertex of every other
            for cc in components[1:]:
                G.add_edge(cc[0], components[0][0])
        return G


    def generate_grid(self, size, dim=False):
        divisors = []
        for i in range(1, size-1):
            if size % i == 0:
                divisors.append((i, size//i))

        m, n = random.choice(divisors)
        grid = Grid2dGraph(m, n)
        # relabel the vertices
        k = 0
        for v in grid.vertices():
            grid.relabel({v:k})
            k += 1

        G = Graph()
        G.add_edges(grid.edges())
        if dim:
            return G, min(m, n)-1
        return G


    def generate_graph(self, size):
        choice = random.randint(0, 6)
        if choice == 0:
            return self.connected(RandomBarabasiAlbert(size, random.randint(1, size-1))), "Barabasi-Albert"
        elif choice == 1:
            return self.connected(RandomGNP(size, self.gnpProb)), "GNP"
        elif choice == 2:
            return self.connected(RandomChordalGraph(size)), "Chordal"
        elif choice == 3:
            return self.connected(RandomTriangulation(size)), "Triangulation"
        elif choice == 4:
            G = Graph()
            G.add_cycle([i for i in range(size)])
            return G, "Cycle"
        elif choice == 5:
            return random_sp(size), "Series-parallel"
        elif choice == 6:
            return self.generate_grid(size), "Grid"
        else:
            return self.generate_graph_length(size, random.randint(size//2)+4), "Length"


    def generate_graph_length(self, min_size, min_length, lb=False):
        """
        generate a graph with treelength at least min_length and at least min_size vertices
        """
        G = Graph()
        G.add_cycle([i for i in range(min_length*3)])
        while len(G.vertices()) < min_size:
            # pick two vertices at random
            u = random.randint(0, len(G.vertices())-1)
            v = random.randint(0, len(G.vertices())-1)
            while u == v:
                v = random.randint(0, len(G.vertices())-1)

            # add a path of length dist(u, v) + 1
            G.add_edge(u, v)
            G.subdivide_edge(u, v, G.distance(u, v))
        if lb:
            return G, min_length
        return G


    def read_graph(self, filename):
        f_in = open(filename, mode='r')
        G = Graph()
        lb_tw, lb_tl = None, None
        for line in f_in:
            e = line.split(",")
            if e[0] == "lb":
                lb_tw = int(e[1])
                lb_tl = int(e[2][:-1])
            else:
                u = int(e[0])
                v = int(e[1])
                G.add_edge(u, v)
        f_in.close()
        return G, lb_tw, lb_tl


    def print_chordals(self, min_size, max_size, step, quantity, dir):
        for i in range(min_size, max_size, step):
            for j in range(quantity):
                G, _, _ = self.read_graph(dir + "/" + str(i) + "-" + str(j) + ".txt")
                if G.is_chordal():
                    print(str(i) + "-" + str(j))


    def test_reduction(self, min_size, max_size, step, decomp_method, quantity, dir, resume=False):
        """
        run tests for decomp_method for graphs in the specified size range and reduces the decomposition
        the test graphs must already be in the dir directory with correct filenames
        """
        if not resume:
            output = open(dir+"/reduce-"+self.names[decomp_method]+".csv", "w")
            output.writelines("size,num,length,reduced_length\n")
            output.close()

        for i in range(min_size, max_size, step):
            for j in range(quantity):
                G, lb_tw, lb_tl = self.read_graph(dir + "/" + str(i) + "-" + str(j)+".txt")
                res = Result()
                self.measure(G, decomp_method, res, 30, measure=True)

                output = open(dir+"/reduce-"+self.names[decomp_method]+".csv", "a")

                if not res.timeout:
                    output.writelines("{},{},{},{}\n".format(i, j, res.length, res.reduced_length))
                else:
                    output.writelines("{},{},null,null\n".format(i, j))
                output.close()
                print("Done with {}-{}\n".format(i, j))


    def start_testing(self, min_size, max_size, step, decomp_method, quantity, dir, timeout=30, resume=False, use_bounds=False):
        """
        run tests for decomp_method for graphs in the specified size range
        the test graphs must already be in the dir directory with correct filenames
        """
        if not resume:
            if not os.path.exists(dir+"/results"):
                os.mkdir(dir+"/results")
            output = open(dir+"/results/"+self.names[decomp_method]+".csv", "w")
            output.writelines("size,num,time,width,length,width_lb,length_lb\n")
            output.close()

        number_of_failures = 0

        for i in range(min_size, max_size, step):
            for j in range(quantity):
                G, lb_tw, lb_tl = self.read_graph(dir + "/files/" + str(i) + "-" + str(j)+".txt")
                res = Result()
                if use_bounds:
                    success, res = self.measure(G, decomp_method, res, timeout, lb_length=lb_tl)
                else:
                    success, res = self.measure(G, decomp_method, res, timeout)

                if not success:
                    number_of_failures += 1

                output = open(dir+"/results/"+self.names[decomp_method]+".csv", "a")
                if not res.timeout:
                    output.writelines("{},{},{},{},{},{},{}\n".format(i, j, res.time, res.width, res.length, lb_tw, lb_tl))
                else:
                    output.writelines("{},{},null,null,null,{},{}\n".format(i, j, lb_tw, lb_tl))
                output.close()

                print("Done with {}-{}\n".format(i, j))
        print("number of failures : {}".format(number_of_failures))

   