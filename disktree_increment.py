# -*- coding: utf-8 -*-

from sage.graphs.graph import Graph
from sage.sets.set import Set
from sage.sets.disjoint_set import DisjointSet
from itertools import combinations, chain
import heapq
import queue
import distance_interface
from functools import total_ordering
from decomp_tools import is_valid_tree_decomposition

__all__ = ['generate_counter_example', 'disk_tree_decomposition', 'is_valid_tree_decomposition']

@total_ordering
class Conflict :
    def __init__(self, u, v, dist):
        self.u = u
        self.v = v
        self.dist = dist

    def __le__(self, other):
        # the order is reversed because python only proposes min heaps
        return self.dist >= other.dist

    def __eq__(self, other):
        # two conflicts are equal if they involve the same vertices
        return (self.u == other.u and self.v == other.v) or (self.u == other.v and self.v == other.u)

    def __str__(self):
        return "conflict: {} and {} at distance {}".format(self.u, self.v, self.dist)

def BFS(G, source):
    """
    Run-of-the-mill breadth first search
    Returns the distances in number of edges from the source to every vertex in the graph
    """
    # the distance from the source
    dist = dict()
    dist[source] = 0

    # the vertices that have already been "touched"
    discovered = set()
    discovered.add(source)

    Q = queue.Queue()
    Q.put(source)
    while not Q.empty():
        u = Q.get()

        for v in G.neighbors(u):
            if v not in discovered:
                dist[v] = dist[u] + 1   # update the distance to v
                Q.put(v)    # we put v in the queue
                discovered.add(v)
    return dist


def out(S, u, G):
    """
    Returns the set of vertices v such that there is a path u w1 ... wn v in G
    with w1 ... wn not in S, u and v in S
    """
    # the connected component of u in V(G) \ (S \ u)
    CC = BFS(G.subgraph(set(G.vertices()).difference(S.difference(set([u])))), u)
    out = set()
    for v in S:
        if set(G.neighbors(v)).intersection(set(CC.keys()).difference(set([u]))) != set():
            out.add(v)
    return out


def ball(G, center, radius, dist, neighbors_c):
    """
    A BFS with limited depth
    Returns the lists of sets of vertices at distance <= radius of center in G and the list of other vertices in the connected component
    """
    CCs = G.connected_components()
    dist_k = []
    new_CCs = []
    for i in range(len(CCs)):
        valid = False
        bk_i = set([center])
        cc_i = set([center])
        for v in CCs[i]:
            if v in neighbors_c:
                valid = True
            if dist[center, v] <= radius:
                bk_i.add(v)
            cc_i.add(v)
        if valid:
            dist_k.append(bk_i)
            new_CCs.append(cc_i)
    return dist_k, new_CCs



def disk_tree_decomposition(G, source=None, lower=0, upper=None, check=True):
    """
    G is an unweighted connected graph
    Uses the disk-tree heuristic to return a tree-decomposition of G
    """
    if len(G.vertices()) == 0:
        H = Graph()
        H.add_vertex(Set())
        return True, H

    # the distances between all pairs of vertices in G
    dist = distance_interface.Distances(G)

    # pick a source if it isn't given
    if source == None:
        source = G.vertices()[0]

    covered = set([source])

    last_valid_decomposition = None
    k = lower-1
    min_conflict = 0
    while last_valid_decomposition == None or not is_valid_tree_decomposition(G, last_valid_decomposition):
        # the set of vertices covered by the tree
        covered = set([source])

        # the tree-decomposition
        TD = Graph()

        # the set of vertices on the border
        border = set()
        if len(G.neighbors(source)) > 0:
            border.add(source)

        # the set of vertices that can be used as the center of a new disk
        C = set()
        C.add(source)

        # the depth of the bag closest to the root that contains each vertex
        depth = dict()
        depth[Set([source])] = 0

        # for each vertex, the "highest" bag that contains it
        B = dict()
        B[source] = Set([source])
        if min_conflict > k:
            k = min_conflict
        else:
            k += 1
        min_conflict = 0
        print("k={}".format(k))
        # while there are uncovered vertices
        while covered != set(G.vertices()):

            #### 1. INIT ####
            # if c \in C doesn't exist then the decomposition failed -> choose a different k
            if C == set():
                break
            c = C.pop()

            # S <- connected component of c in V(G) \ (covered \ c)
            added_bags = []
            Bks, CCs = ball(G.subgraph(set(G.vertices()).difference(covered)), c, k, dist, set(G.neighbors(c)))
            for i in range(len(Bks)):
                Bk, CC = Bks[i], CCs[i]
                S = Bk # the ball of size k centered in c with no covered vertices except c
                # we add all the vertices in the border that have a neighbor in CC
                for v in border:
                    if not set(G.neighbors(v)).intersection(CC) == set():
                        S.add(v)

                #print("\tS = {}".format(S))
                #### 2. REDUCE #####
                # store the conflicts in a sorted heap to always have the maximum ready
                conflicts = []
                heapq.heapify(conflicts)
                # find the conflicts in S and sort them in the heap
                keep_going = True
                for u in S:
                    for v in out(covered.union(S), u, G):
                        if dist[u, v] > k:
                            # if there is a conflict between two vertices in the border, we need to choose another center
                            if u in border and v in border:
                                keep_going = False
                            elif u in S and v in S:
                                new_conflict = Conflict(u, v, dist[u, v])
                                heapq.heappush(conflicts, new_conflict)

                # there was a conflict between vertices of the border so we go to the next c
                if not keep_going:
                    continue

                # selectively remove vertices in conflict

                while len(conflicts) > 0:
                    max_conflict = heapq.heappop(conflicts)
                    if max_conflict.v in border or (max_conflict.u not in border and\
                     dist[c, max_conflict.u] >= dist[c, max_conflict.v]):
                        to_remove = max_conflict.u
                    else:
                        to_remove = max_conflict.v
                    if to_remove in S:
                        S.remove(to_remove)
                        print("removed {} (conflict between {} and {})".format(to_remove, max_conflict.u, max_conflict.v))

                    # check if there are conflicts in the vertices left in S
                    new_conflicts = []
                    for conf in conflicts:
                        if conf.u != to_remove and conf.v != to_remove:
                            heapq.heappush(new_conflicts, conf)

                    # look for new conflicts involving the neighbors in S of the removed vertex
                    for w in set(G.neighbors(to_remove)).intersection(S):
                        for v in out(covered.union(S), w, G):
                            if dist[w, v] > k:
                                # if there is a conflict between two vertices in the border, we need to choose another center
                                if w in border and v in border:
                                    keep_going = False
                                    break
                                elif w in S and v in S:
                                    new_conflict = Conflict(w, v, dist[w, v])
                                    heapq.heappush(new_conflicts, new_conflict)
                        if not keep_going:
                            break
                    conflicts = new_conflicts
                    if not keep_going:
                        break
                # there was a conflict between vertices of the border so we go to the next c
                if not keep_going:
                    continue

                #### 3. Update ####
                if not S.issubset(covered):
                    added_bags.append(S)
                    # add S to the tree
                    TD.add_edge(Set(B[c]), Set(S))
                    # print("added edge {} - {}".format(Set(B[c]), Set(S)))
                    depth[Set(S)] = depth[Set(B[c])] + 1

                    # the covered vertices now include the vertices of S
                    covered = covered.union(S)
                    #print("\tcovered = {}".format(covered))
                    # for each vertex v in S, compute B(s)
                    for v in S:
                        # if v is not yet in a bag, then S is the closest bag to the root
                        if v not in B:
                            B[v] = Set(S)
                        else:   # otherwise we have to choose between S and the former B
                            if depth[Set(B[v])] > depth[Set(S)]:
                                B[v] = Set(S)
                    #print("\tborder = {}".format(border))

            if len(added_bags) > 0:
                border = set([v for v in border if set(G.neighbors(v)).difference(covered) != set()])
                for v in covered:
                    # then add the ones that are now part of the border
                    if set(G.neighbors(v)).difference(covered) != set():
                        border.add(v)
                # the vertices of C are the ones on the new border with maximum depth
                new_C = set([source])
                max_depth = 0
                for v in border:
                    if depth[Set(B[v])] > max_depth:
                        new_C = set([v])
                        max_depth = depth[Set(B[v])]
                    elif depth[Set(B[v])] == max_depth:
                        new_C.add(v)
                C = new_C
            if covered == set(G.vertices()):
                last_valid_decomposition = Graph(TD)

    return True, last_valid_decomposition


def generate_counter_example(i):
    """
    i is the length of the paths
    """
    # the backbone of the counter example
    G = Graph()
    G.add_edge(0, 1)
    G.add_edge(0, 11)
    G.add_edge(1, 11)
    G.add_edge(1, 5)
    G.add_edge(1, 2)
    G.add_edge(2, 3)
    G.add_edge(3, 4)
    G.add_edge(4, 5)
    G.add_edge(4, 6)
    G.add_edge(5, 6)
    G.add_edge(6, 7)
    G.add_edge(6, 8)
    G.add_edge(7, 8)
    G.add_edge(7, 9)
    G.add_edge(9, 10)
    G.add_edge(10, 12)
    G.add_edge(12, 11)
    G.add_edge(11, 8)

    G.subdivide_edge(3, 4, 1)
    G.subdivide_edge(1, 11, 1)

    # change the edges to paths of length i
    for e in G.edges():
        G.subdivide_edge(e[0], e[1], i-1)

    return G
