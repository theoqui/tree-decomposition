# -*- coding: utf-8 -*-

# functions designed to automate the analysis of the results from the testing machine

import pandas as pd
import matplotlib.pyplot as plt

def get_graph_list(filename):
    f_in = open(filename, mode='r')
    graphs = set()
    for line in f_in:
        data = line[:-5].split('-')
        graphs.add((int(data[0]), int(data[1])))
    f_in.close()
    return graphs


def get_chordals():
    return get_graph_list("second_tests/info/INFO-Chordal.txt")


def extract_data(filename):
    """
    load the .csv file and compute a lower bound on the treewidth and treelength of each graph
    """
    df = pd.read_csv(filename)
    return df


def get_columns(df, columns):
    df1 = df[columns]
    return df1


def get_computed_length(df):
    """
    returns the dataframe with only the graphs and their computed length
    """
    df1 = df[['size','num','length']]
    return df1


def get_computed_width(df):
    """
    returns the dataframe with only the graphs and their computed width
    """
    df1 = df[['size','num','width']]
    return df1


def get_execution_time(df):
    """
    returns the dataframe with only the graphs and the execution time
    """
    df1 = df[['size','num','time']]
    return df1


def get_lower_bounds(df):
    """
    returns the dataframe with only the graphs and their lower bounds
    """
    df1 = df[['size','num','width_lb', 'length_lb']]
    return df1


def average(df, column):
    """
    input : one of the dataframes computed above (only tree columns)
    returns the dataframe in which the values in column have been averaged for each given size
    """
    res = pd.DataFrame(columns={0, 1})
    rows = [dict() for i in range(len(df['size'].unique()))]
    i = 0
    for k in df['size'].unique():
        df1 = df.loc[df['size'] == k]    # the graphs of size k
        df1 = df1[df1[column].notnull()]
        avg = df1[column].mean()
        rows[i]['size'] = k
        rows[i][column] = avg
        i += 1
    return pd.DataFrame(rows)


def min_keys(D):
    min_key = list(D.keys())[0]
    min_value = list(D.values())[0]
    keys = [min_key]
    for k, v in D.items():
        if v == min_value:
            keys.append(k)
        if v < min_value:
            keys = [k]
            min_value = v
    return set(keys)


def min_columns(row, columns):
    min_cols = [columns[0]]
    min_value = row[columns[0]]
    for col in columns[1:]:
        if row[col] == min_value:
            min_cols.append(col)
        if row[col] < min_value:
            min_cols = [col]
    return min_cols


def width_length_score(dfs):
    """
    input : a dictionary {'algo_name':df}
    """
    first_df = list(dfs.values())[0]
    size = first_df['size'].unique()
    nb = first_df['num'].unique()
    width_scores = dict()
    length_scores = dict()

    print("size : {} - nb : {}".format(size, nb))

    for label in dfs.keys():
        width_scores[label] = 0
        length_scores[label] = 0

    for i in size:
        for j in nb:
            width = dict()
            length = dict()
            for algo in dfs.keys():
                df1 = dfs[algo]
                df2 = df1.loc[df1['size'] == i]
                df3 = df2.loc[df2['num'] == j]
                width[algo] = list(df3['width'])[0]
                length[algo] = list(df3['length'])[0]

            best_width = min_keys(width)
            best_length = min_keys(length)

            for algo in best_width:
                width_scores[algo] += 1
            for algo in best_length:
                length_scores[algo] += 1
    return width_scores, length_scores


def time_graph(dfs):
    """
    input : a dictionary {'algo name':df}
    """
    first_df = list(dfs.values())[0]
    new_df = {'size':pd.Series(first_df['size'].unique())}
    for k, v in dfs.items():
        new_df[k] = average(get_execution_time(v), 'time')['time']
    df = pd.DataFrame(new_df)
    ax = df.plot(x='size')
    ax.set_ylabel("average execution time by size (s)")
    plt.show()


def length_graph(dfs):
    """
    input : a dict {'algo name':df}
    """
    first_df = list(dfs.values())[0]
    new_df = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    for k, v in dfs.items():
        new_df[k] = get_computed_length(v)['length']
    df = pd.DataFrame(new_df)
    avg = []
    for index, row in df.iterrows():
        sum = 0
        for label in dfs.keys():
            sum += row[label]
        avg.append(sum / len(dfs.keys()))
    df['average'] = avg
    df['id'] = df.apply(lambda row: "{}-{}".format(int(row['size']), int(row['num'])),axis=1)
    df.sort_values('average', inplace=True)
    df = df[['id']+list(dfs.keys())]
    ax = df.plot.bar(x='id')
    plt.xticks(fontsize=6)
    ax.set_ylabel('computed length')
    plt.show()


def graph_time(dfs):
    """
    input : a dict {'algo':df}
    """
    first_df = list(dfs.values())[0]
    new_df = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    for k, v in dfs.items():
        new_df[k] = get_execution_time(v)['time']
    df = pd.DataFrame(new_df)
    avg = []
    for index, row in df.iterrows():
        sum = 0
        for label in dfs.keys():
            sum += row[label]
        avg.append(sum / len(dfs.keys()))
    df['average'] = avg
    df['id'] = df.apply(lambda row: "{}-{}".format(int(row['size']), int(row['num'])),axis=1)
    df.sort_values('average', inplace=True)
    df = df[['id']+list(dfs.keys())]
    ax = df.plot.bar(x='id')
    ax.set_ylabel('execution time (s)')
    plt.show()


def size_length(dfs):
    """
    input : a dict {'algo':df}
    """
    first_df = list(dfs.values())[0]
    new_df = {'size':pd.Series(first_df['size'].unique())}
    for k, v in dfs.items():
        new_df[k] = average(get_computed_length(v), 'length')['length']
    df = pd.DataFrame(new_df)
    ax = df.plot(x='size', alpha=0.5)
    ax.set_ylabel("average length")
    plt.show()


def length_nbgraphs(dfs):
    lengths = dict()
    for df in dfs.values():
        for key, row in df.iterrows():
            k = float(row['length'])
            if not pd.isna(k):
                lengths[int(k)] = lengths.get(int(k), 0) + 1
    pre = dict()
    i = 0
    for key, value in lengths.items():
        pre[i] = [key, value]
        i += 1
    df = pd.DataFrame.from_dict(pre, orient='index', columns=['length', 'count'])
    df.sort_values('count', ascending=False, inplace=True)
    ax = df.plot.bar(x='length')
    ax.set_ylabel("count")
    plt.show()


def length_best(dfs):
    """
    input : a dict {'algo':df}
    """
    # number of times algo1 was better than algo2 : better_than[algo1][algo2]
    better_than = {k:dict() for k in dfs.keys()}
    for k in dfs.keys():
        better_than[k] = {k:0 for k in dfs.keys()}

    first_df = list(dfs.values())[0]
    new_df = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    for k, v in dfs.items():
        new_df[k] = get_computed_length(v)['length']
    df = pd.DataFrame(new_df)

    for index, row in df.iterrows():
        for algo1 in dfs.keys():
            for algo2 in dfs.keys():
                if not algo1 == algo2:
                    if row[algo1] > row[algo2]:
                        better_than[algo1][algo2] += 1

    print(better_than)

    df = pd.DataFrame(better_than)
    ax = df.plot.bar()
    ax.set_ylabel("count")
    plt.title("Number of times the algorithm on the x axis gave a (strictly) smaller length than the others")
    plt.show()


def count_bestlength(dfs):
    """
    input : {'algo':df}
    """
    chordals = get_chordals()

    first_df = list(dfs.values())[0]
    df1 = pd.DataFrame({'size':first_df['size'], 'num':first_df['num'], 'chordal':False})

    # for each graph, know if it is chordal
    df1['chordal'] = df1.apply(lambda row: (row['size'], row['num']) in chordals, axis=1)

    for key, df in dfs.items():
        df1[key] = df['length']

    # for each length i, create a dataframe with the graphs such that the best computed length was i
    dict_lengths = dict()
    for key, row in df1.iterrows():
        min_cols = min_columns(row, list(dfs.keys()))
        if row[min_cols[0]] in dict_lengths:
            dict_lengths[row[min_cols[0]]].append(row)
        else:
            dict_lengths[row[min_cols[0]]] = [row]

    dataframes = {i:pd.DataFrame(dict_lengths[i]) for i in dict_lengths.keys()}
    rows = []
    for i in dataframes.keys():
        if pd.isna(i):
            continue
        df = dataframes[i]

        row = dict()
        row['length'] = int(i)
        row['Number of graphs for which this is the best computed length'] = len(df.index)
        row['Number of chordal graphs'] = len(df.loc[df['chordal'] == True].index)

        comparisons = set()
        algos = list(dfs.keys())
        for i in range(len(algos)-1):
            algo1 = algos[i]
            for algo2 in algos[i+1:]:
                comparisons.add((algo1, algo2))
                comparisons.add((algo2, algo1))


        for algo1, algo2 in comparisons:
            label = 'length({}) < length({})'.format(algo1, algo2)
            row[label] = 0
            for id, r in df.iterrows():
                if r[algo1] < r[algo2]:
                    row[label] += 1

        rows.append(row)

    # putting the columns in the right order
    res = pd.DataFrame(rows)
    cols = res.columns.tolist()
    cols.remove('length')
    cols.remove('Number of chordal graphs')
    cols.remove('Number of graphs for which this is the best computed length')
    cols = ['length', 'Number of graphs for which this is the best computed length', 'Number of chordal graphs'] + cols
    res = res[cols]

    # plotting
    ax = res.plot.bar(x='length', rot=0)
    ax.set_ylabel("count")
    #plt.xticks(fontsize=8)
    plt.show()
    return df1

def scatter_time(name1, df1, name2, df2):
    """
    dfs must contain only two dataframes
    """
    df = pd.DataFrame()
    df['size'] = df1['size']
    df['num'] = df1['num']
    df['time ' + name1] = df1['time']
    df['length ' + name1] = df1['length']
    df['time ' + name2] = df2['time']
    df['length ' + name2] = df2['length']
    ax = df.plot.scatter(x='time '+name1, y='time '+name2, s=df['length '+name1])
    plt.show()

def make_virtual_best(dfs):
    """
    input : dict of dataframes
    output : the virtual best among the dataframes
    """
    first_df = list(dfs.values())[0]
    vba = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    vba_df = pd.DataFrame(vba)
    best_length = []
    best_time = []
    for index, row in vba_df.iterrows():
        rows = [df.iloc[[index]] for df in dfs.values()]
        length = 1000000
        time = 100000
        for row in rows:
            if row.at[index, 'length'] < length:
                length = row.at[index, 'length']
            if row.at[index, 'time'] < time:
                time = row.at[index, 'time']
        best_length.append(length)
        best_time.append(time)
    vba_df['length'] = best_length
    vba_df['time'] = best_time
    return vba_df


######################

def read_data(path):
    bfs = extract_data(path+"/results/bfs layering.csv")
    dtd = extract_data(path+"/results/disk-tree (dichotomy).csv")
    dti = extract_data(path+"/results/disk-tree (incremental).csv")
    ut = extract_data(path+"/results/uncle-trees.csv")
    lexm = extract_data(path+"/results/lexM.csv")
    res = {'lex-m':lexm, 'bfs layering':bfs, 'uncle-trees':ut, 'disk-tree (dichotomy)':dtd, 'disk-tree (increment)':dti}
    #res = {'lex-m':lexm, 'bfs layering':bfs, 'uncle-trees':ut}
    return res


def select_graphs(dfs, filename):
    graphs = get_graph_list(filename)
    dict = {}
    for algo, df in dfs.items():
        rows = []
        for index, row in df.iterrows():
            if (row['size'], row['num']) in graphs:
                rows.append(row)
        dict[algo] = pd.DataFrame(rows)
    return dict


def graph1():
    """
    draw the graph comparing exec time for different algorithms
    """
    res = read_data(PATH)
    time_graph(res)

def graph2():
    """
    bar graph of computed lengths
    """
    res = read_data(PATH)
    length_graph(res)

def graph3():
    """
    bar graph : x = graphs, y = execution time
    """
    res = read_data(PATH)
    graph_time(res)

def graph4():
    """
    graph : x = size, y = average length
    """
    res = read_data(PATH)
    size_length(res)

def graph5():
    """
    bar graph : x = length, y = number of times this length has been computed by any of the 3 algorithms
    """
    res = read_data(PATH)
    length_nbgraphs2(res)

def graph6():
    """
    bar graph : x = algo1, y = number of times algo1 was better than algo2
    """
    res = read_data(PATH)
    length_best(res)

def graph7():
    """
    bar graph : x = lengths, y = numer of times this length was the best computed length, number of times algo1 was better than algo2
    """
    res = read_data(PATH)
    count_bestlength(res)


def graph8():
    """
    bar graphs : x = length, y = exec_time
    """
    fig, axes = plt.subplots(nrows=2, ncols = 2)
    dfs = read_data(PATH)
    i = 0
    j = 0
    for key, df in dfs.items():
        if i > 1:
            i = 0
            j += 1
        df.sort_values('time', inplace=True)
        df = df[['time', 'length']]
        df.plot.bar(x='length', ax=axes[i, j], rot=0)
        i += 1
    plt.show()


def compare_vb():
    """
    scatter plot to compare every algorithme to the virtual best
    """
    dfs = read_data(PATH)
    dfs['virtual best'] = make_virtual_best(dfs)
    # colors
    colors = {'lex-m':'blue', 'bfs layering':'orange', 'uncle-trees':'green', 'disk-tree (dichotomy)':'red', 'disk-tree (increment)':'purple', 'virtual best':'grey'}
    ax = None
    x = 0
    y = 0
    limit_x = 22
    limit_y_fast = 2.5
    limit_y_slow = 95
    xscale = (0.5, limit_x)
    yscale = {'lex-m':(0, limit_y_fast), 'bfs layering':(0, limit_y_fast), 'uncle-trees':(0, limit_y_fast), 'disk-tree (dichotomy)':(0, limit_y_slow), 'disk-tree (increment)':(0, limit_y_slow), 'virtual best':(0, limit_y_fast)}
    fig, axes = plt.subplots(nrows=2, ncols=3)
    for key, value in dfs.items():
        axes[x, y].set(xlim=xscale, ylim=yscale[key])
        value.plot(kind='scatter', x='length', y='time', label=key, color=colors[key], ax=axes[x, y])
        y+=1
        if y > 2:
            y = 0
            x += 1
    plt.show()


def stat_reduction(file):
    df = pd.read_csv(file)
    df['delta'] = df.apply(lambda row: row['length'] - row['reduced_length'], axis=1)
    res = dict()
    res['max'] = df['delta'].max()
    res['min'] = df['delta'].min()
    res['avg'] = df['delta'].loc[df['delta'] != 0].mean()
    res['changed'] = len(df.loc[df['delta'] != 0].index)
    return res


def max_degree_graph(max_degrees):
    df = pd.DataFrame.from_dict(max_degrees, orient="index")
    df.columns = ["count"]
    df.sort_values('count')
    df.plot.bar()
    plt.xlabel("Degree")
    plt.ylabel("Count")
    plt.show()


def diameter_graph(diameters):
    df = pd.DataFrame.from_dict(diameters, orient="index")
    df.columns = ["count"]
    df.sort_values('count')
    df.plot.bar()
    plt.xlabel("Diameter")
    plt.ylabel("Count")
    plt.show()


def best_length_comparison(dfs):
    first_df = list(dfs.values())[0]
    lengths = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    lengths = pd.DataFrame(lengths)
    distinct_best_lengths = set()

    for algo, df in dfs.items():
        lengths[algo] = df['length']

    times_best = {key:dict() for key in dfs.keys()} # algo -> length -> times_best, times_only_best

    for index, row in lengths.iterrows():
        min_cols = min_columns(row, list(dfs.keys()))
        min_length = row[min_cols[0]]
        distinct_best_lengths.add(min_length)

        for algo in min_cols:
            if min_length not in times_best[algo]:
                times_best[algo][min_length] = [0, 0]
            times_best[algo][min_length][0] += 1
        if len(min_cols) == 1:
            times_best[min_cols[0]][min_length][1] += 1

    return times_best, distinct_best_lengths


def best_length_comparison_in(dfs, bounds):
    first_df = list(dfs.values())[0]
    lengths = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    lengths = pd.DataFrame(lengths)
    distinct_best_lengths = set()

    for algo, df in dfs.items():
        lengths[algo] = df['length']

    times_best = {key:dict() for key in dfs.keys()} # algo -> length -> times_best, times_only_best

    for index, row in lengths.iterrows():
        min_cols = min_columns(row, list(dfs.keys()))
        min_length = row[min_cols[0]]
        if min_length < bounds[0] or min_length >= bounds[1]:
            continue
        distinct_best_lengths.add(min_length)

        for algo in min_cols:
            if min_length not in times_best[algo]:
                times_best[algo][min_length] = [0, 0]
            times_best[algo][min_length][0] += 1
        if len(min_cols) == 1 or (len(min_cols) == 2 and 'disk-tree (dichotomy)' in min_cols and 'disk-tree (increment)' in min_cols):
            for i in range(len(min_cols)):
                times_best[min_cols[i]][min_length][1] += 1

    return times_best, distinct_best_lengths


def best_length_comparison_interval(dfs, interval_size=10):

    def make_interval(k):
        return (k - k%interval_size, k-k%interval_size + interval_size)

    first_df = list(dfs.values())[0]
    lengths = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    lengths = pd.DataFrame(lengths)
    distinct_best_lengths = set()

    for algo, df in dfs.items():
        lengths[algo] = df['length']

    times_best = {key:dict() for key in dfs.keys()} # algo -> length interval -> times_best, times_only_best

    for index, row in lengths.iterrows():
        min_cols = min_columns(row, list(dfs.keys()))
        min_length = make_interval(row[min_cols[0]])
        distinct_best_lengths.add(make_interval(row[min_cols[0]]))

        for algo in min_cols:
            if min_length not in times_best[algo]:
                times_best[algo][min_length] = [0, 0]
            times_best[algo][min_length][0] += 1
        if len(min_cols) == 1 or (len(min_cols) == 2 and 'disk-tree (dichotomy)' in min_cols and 'disk-tree (increment)' in min_cols):
            for i in range(len(min_cols)):
                times_best[min_cols[i]][min_length][1] += 1

    return times_best, distinct_best_lengths


def best_length_comparison_interval_in(dfs, bounds, interval_size=10):

    def make_interval(k):
        return (k - k%interval_size, k-k%interval_size + interval_size)

    first_df = list(dfs.values())[0]
    lengths = {'size':pd.Series(first_df['size']), 'num':pd.Series(first_df['num'])}
    lengths = pd.DataFrame(lengths)
    distinct_best_lengths = set()

    for algo, df in dfs.items():
        lengths[algo] = df['length']

    times_best = {key:dict() for key in dfs.keys()} # algo -> length interval -> times_best, times_only_best

    for index, row in lengths.iterrows():
        min_cols = min_columns(row, list(dfs.keys()))
        min_length = make_interval(row[min_cols[0]])
        if row[min_cols[0]] < bounds[0] or row[min_cols[0]] >= bounds[1]:
            continue
        distinct_best_lengths.add(make_interval(row[min_cols[0]]))

        for algo in min_cols:
            if min_length not in times_best[algo]:
                times_best[algo][min_length] = [0, 0]
            times_best[algo][min_length][0] += 1
        if len(min_cols) == 1:
            times_best[min_cols[0]][min_length][1] += 1

    return times_best, distinct_best_lengths


def make_latex_table(bounds=None):
    dfs = read_data(PATH)
    if bounds == None:
        print("THIS WORKS")
        times_best, distinct_best_lengths = best_length_comparison(dfs)
    else:
        print("THIS DOESN'T WORK")
        times_best, distinct_best_lengths = best_length_comparison_in(dfs, bounds)
    distinct_best_lengths = list(distinct_best_lengths)
    distinct_best_lengths.sort()
    col_format = "cc|"
    width = len(distinct_best_lengths) + 2

    # generate the column format
    for i in range(len(distinct_best_lengths)):
        col_format = col_format+"c|"

    # generate the top of the table
    tab = """
\\begin{{tabular}}{{{1}}}
\\cline{{3-{2}}}
& & \\multicolumn{{{0}}}{{c|}}{{meilleures lengths}} \\\\ \\cline{{3-{2}}}
""".format(width-2, col_format, width)
    line = "& "
    for i in distinct_best_lengths:
        line = line + "& {} ".format(int(i))
    line = line + "\\\\ \\cline{{1-{}}}\n".format(width)
    tab = tab+line

    # generate the lines
    for algo in dfs.keys():
        line = """
\\multicolumn{{1}}{{|c}}{{\\multirow{{2}}{{*}}{{{0}}} }} &
\\multicolumn{{1}}{{|c|}}{{\\# trouvée}} """.format(algo)
        for i in range(len(distinct_best_lengths)):
            line = line + "& {} ".format(int(times_best[algo].get(distinct_best_lengths[i], [0, 0])[0]))
        line = line + """\\\\ \\cline{{2-{0}}}
""".format(width)
        line = line + """
\\multicolumn{1}{|c|}{}                        &
\\multicolumn{1}{|c|}{\\# trouvée seul} """
        for i in range(len(distinct_best_lengths)):
            line = line + "& {} ".format(int(times_best[algo].get(distinct_best_lengths[i], [0, 0])[1]))
        line = line + """\\\\ \\cline{{1-{}}}
""".format(width)
        tab = tab+line
    tab = tab+"\\end{tabular}"
    print(tab)


def make_latex_table_intervals(interval_size=10, bounds=None):
    dfs = read_data(PATH)
    if bounds == None:
        times_best, distinct_best_lengths = best_length_comparison_interval(dfs, interval_size=interval_size)
    else:
        times_best, distinct_best_lengths = best_length_comparison_interval_in(dfs, bounds=bounds)
    distinct_best_lengths = list(distinct_best_lengths)
    distinct_best_lengths.sort()
    col_format = "cc|"
    width = len(distinct_best_lengths) + 2

    # generate the column format
    for i in range(len(distinct_best_lengths)):
        col_format = col_format+"c|"

    # generate the top of the table
    tab = """
\\begin{{tabular}}{{{1}}}
\\cline{{3-{2}}}
& & \\multicolumn{{{0}}}{{c|}}{{meilleures lengths}} \\\\ \\cline{{3-{2}}}
""".format(width-2, col_format, width)
    line = "& "
    for i in distinct_best_lengths:
        line = line + "& [{},{}[ ".format(int(i[0]), int(i[1]))
    line = line + "\\\\ \\cline{{1-{}}}\n".format(width)
    tab = tab+line

    # generate the lines
    for algo in dfs.keys():
        line = """
\\multicolumn{{1}}{{|c}}{{\\multirow{{2}}{{*}}{{{0}}} }} &
\\multicolumn{{1}}{{|c|}}{{\\# trouvée}} """.format(algo)
        for i in range(len(distinct_best_lengths)):
            line = line + "& {} ".format(int(times_best[algo].get(distinct_best_lengths[i], [0, 0])[0]))
        line = line + """\\\\ \\cline{{2-{0}}}
""".format(width)
        line = line + """
\\multicolumn{1}{|c|}{}                        &
\\multicolumn{1}{|c|}{\\# trouvée seul} """
        for i in range(len(distinct_best_lengths)):
            line = line + "& {} ".format(int(times_best[algo].get(distinct_best_lengths[i], [0, 0])[1]))
        line = line + """\\\\ \\cline{{1-{}}}
""".format(width)
        tab = tab+line
    tab = tab+"\\end{tabular}"
    print(tab)


def select_length_interval(df, min, max, algos=['lex-m', 'bfs layering', 'uncle-trees', 'disk-tree (dichotomy)', 'disk-tree (increment)']):
    rows = []
    distinct_best_lengths = set()

    for index, row in df.iterrows():
        min_cols = min_columns(row, algos)
        if row[min_cols[0]] >= min and row[min_cols[0]] < max:
            rows.append(row)
            distinct_best_lengths.add(row[min_cols[0]])

    return pd.DataFrame(rows)

PATH="third_tests/seriesparallel"
