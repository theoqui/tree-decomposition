from sage.graphs.graph import Graph
from sage.sets.set import Set
from sage.sets.disjoint_set import DisjointSet
from itertools import combinations, chain
import queue
from decomp_tools import is_valid_tree_decomposition

def merge(G, vertices):
    """
    Merges the given vertices in G and returns the resulting vertex
    """
    list_vertices = list(vertices)
    G.merge_vertices(list_vertices)
    return list_vertices[0]


def make_layering_tree(G, source=None, check=True):
    """
    Given a connected non-empty unweighted graph G, returns a layering tree of G from source
    If source is None then we pick any vertex
    """

    if check:
        if not G.is_connected():
            raise ValueError("The input graph is not connected")

        if len(G.vertices()) == 0:
            raise ValueError("The input graph is empty")

    if source == None:  # pick any vertex of the graph if the source is not given
        source = G.vertices()[0]

    ### Do a BFS from source to get the layers

    # the BFS tree
    T = dict()

    # the layers
    layers = dict()
    layers[0] = set([source])

    # the distance from source
    dist = dict()
    dist[source] = 0

    # the vertices that have already been "touched"
    discovered = set()
    discovered.add(source)
    max_layer = 0

    Q = queue.Queue()
    Q.put(source)
    while not Q.empty():
        u = Q.get()

        for v in G.neighbors(u):
            if v not in discovered:
                T[v] = u    # add v to the tree
                dist[v] = dist[u] + 1   # update the distance to v
                if dist[v] > max_layer: # if we need to put v in a new layer
                    max_layer += 1
                    layers[max_layer] = set()
                #if dist[v] not in layers:
                #    layers[dist[v]] = set()
                layers[dist[v]].add(v)   # put v in the right layer
                Q.put(v)    # we put v in the queue
                discovered.add(v)
    ### build the layering-tree

    # copy the graph
    G1 = G.copy()

    # the layering tree : bag -> parent
    H = dict()

    # the correspondance between a vertex and the bag that was merged into it
    compression = dict()

    # the correspondance between a layer and its partitions
    bags = dict()
    bags[max_layer] = G1.subgraph(layers[max_layer]).connected_components()

    #the vertices to consider in the next connected components
    previous = list()

    # merge the connected components of the outer layer into vertices
    for component in bags[max_layer]:
        v = merge(G1, component)
        compression[v] = set(component)
        previous.append(v)

    # for each subsequent layer, compute the connected components including the previously computed ones
    for i in range(max_layer-1, 0, -1):
        bags[i] = G1.subgraph(layers[i].union(previous)).connected_components()
        # connect the bags
        for j in range(len(bags[i])):
            connect_to = set(bags[i][j]).intersection(previous).intersection(layers[i+1])  # the bags to connect to this one
            bags[i][j] = set(bags[i][j]).intersection(layers[i]) # the actual bag
            v = merge(G1, bags[i][j])
            compression[v] = bags[i][j]
            previous.append(v)

            for child in connect_to:
                H[Set(compression[child])] = Set(bags[i][j])
    # connect the root to the bags of the first layer
    if max_layer >= 1:
        for bag in bags[1]:
             H[Set(bag)] = Set([source])

    return H


def bfs_layering_decomposition(G, check=True, source=None):
    """
    Does a bfs-layering tree decomposition of G from vertex source
    """
    if check:
        if not G.is_connected():
            raise ValueError("The input graph is not connected")

    if len(G.vertices()) == 0:  # if the graph is empty, return it
        H = Graph()
        H.add_vertex(Set())
        return True, H

    if source == None:  # pick any vertex of the graph if the source is not given
        source = G.vertices()[0]

    # build the layering tree and get the leaves
    print("building the layering tree...")
    T = make_layering_tree(G, check=check, source=source)
    print("Done")
    print("Computing the leaves")
    leaves = set(T.keys()).difference(set(T.values()))
    print("Done")

    # the tree-decomposition
    H = Graph(name="BFS-Layering decomposition")

    # to make the computation easier, we add an empty bag above the source
    #T[Set([source])] = Set()

    # association between the pre-bags and the actual bags
    bags = {Set([source]):Set([source])}

    # we use a queue to go up the tree
    Q = queue.Queue()
    for leaf in leaves:
        Q.put(leaf)

    while not Q.empty():
        current_bag = Set(Q.get())
        # for all bags except the source we take the union with the parent:
        if current_bag == Set([source]):
            H.add_vertex(current_bag)
        else:
            # add the vertices of the parent adjacent to those of the bag in G
            neighbors_in_parent = set()
            for u in current_bag:
                for v in G.neighbors(u):
                    if v in T[current_bag]:
                        neighbors_in_parent.add(v)
            bags[current_bag] = current_bag.union(Set(neighbors_in_parent))

            # add the parent to the queue
            Q.put(T[current_bag])
    for child, parent in T.items():
        H.add_edge(bags[child], bags[parent])

    if check and not is_valid_tree_decomposition(G, H):
            print("The decomposition failed")
            return False, H

    return True, H


def test():
    G = Graph()
    G.add_edge(0,1)
    G.add_edge(0,2)
    G.add_edge(1,2)
    G.add_edge(1,3)
    G.add_edge(2,4)
    G.add_edge(3,5)
    G.add_edge(4,6)
    G.add_edge(4,7)
    G.add_edge(5,8)
    G.add_edge(5,9)
    G.add_edge(6,10)
    G.add_edge(6,11)
    G.add_edge(7,12)
    G.add_edge(9,10)
    G.add_edge(8,13)
    G.add_edge(9,13)
    G.add_edge(11,14)
    G.add_edge(12,14)

    H, L = make_layering_tree(G)

    return G, H
