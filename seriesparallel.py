from sage.graphs.graph import Graph
import random

def random_sp(size):
    saved = dict()
    G = Graph()
    G.add_edge(0, 1)
    source_G = 0
    dest_G = 1

    saved[2] = G.copy(), source_G, dest_G
    while len(G.vertices()) < size:
        # the possible choices for the new piece to add to G
        possible_sizes = [i for i in saved.keys() if i <= size - len(G.vertices())]
        if len(possible_sizes) == 0:
            possible_sizes = [2]

        # choose a graph to stick to G
        choice = random.choice(possible_sizes)
        H, source_H, dest_H = saved[choice]
        H = H.copy()

        # randomly choose a parallel of series composition
        if random.randint(0, 1) == 0:
            # parallel composition
            if G.has_edge(source_G, dest_G) and H.has_edge(source_H, dest_H):
                H.subdivide_edge(source_H, dest_H, 1)

            # relabel the vertices of H before adding them to G
            source_H += len(G.vertices()) + 1
            dest_H += len(G.vertices()) + 1
            H.relabel({i:len(G.vertices())+i+1 for i in H.vertices()})
            G.add_edges(H.edges())
            G.merge_vertices([source_G, source_H])
            G.merge_vertices([dest_G, dest_H])
        else:
            # series composition
            source_H += len(G.vertices()) + 1
            dest_H += len(G.vertices()) + 1
            H.relabel({i:len(G.vertices())+i+1 for i in H.vertices()})
            G.add_edges(H.edges())

            G.merge_vertices([dest_G, source_H])
            dest_G = dest_H

        # then re-label the vertices of G
        k = 0
        for i in G.vertices():
            if i == source_G:
                source_G = k
            if i == dest_G:
                dest_G = k
            G.relabel({i:k})
            k += 1
        saved[len(G.vertices())] = G.copy(), source_G, dest_G
    return G
