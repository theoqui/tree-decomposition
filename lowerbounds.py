# -*- coding: utf-8 -*-

from sage.sets.set import Set
from sage.graphs.graph import Graph


def max_clique(G):
    """
    computes the size of the largest clique in G
    """
    return G.clique_number()


def longest_isometric_cycle(G):
    """
    computes the longest isometric cycle of G
    """

    def unpack_set(S):
        """
        unpack a set of 2 values
        """
        S_list = S.list()
        return S_list[0], S_list[1]


    def power(G, k):
        """
        computes G power k
        """
        G_pow_k = Graph()
        G_pow_k.add_vertices(G.vertices())
        dist = G.distance_all_pairs()

        for u in G.vertices():
            for v in G.vertices():
                dist_u_v = dist[u].get(v, None)
                if not u == v and not dist_u_v == None and dist_u_v <= k: # if v isn't reachable from u, we say the distance is one more than the number of edges (~ +inf)
                    G_pow_k.add_edge(u, v)
        return G_pow_k

    #### the actual algorithm starts here
    ans = 0
    if G.is_tree():
        return 0
    dist = G.distance_all_pairs()
    for k in range(3, len(G.vertices())+1):
        Vk = set()
        for u in G.vertices():
            for v in G.vertices():
                if dist[u][v] == k // 2:
                    Vk.add((u, v))

        Ek = set()
        for u, v in Vk:
            for w, x in Vk:
                if w in G.neighbors(u) and x in G.neighbors(v):
                    Ek.add(((u,v), (w,x)))

        Gk = Graph()
        Gk.add_vertices(Vk)
        Gk.add_edges(Ek)
        Gk_pow_k = power(Gk, k//2)
        for u in G.vertices():
            for v in G.vertices():
                for x in G.vertices():
                    if (u, v) in Vk \
                    and ((u == x and k % 2 == 0) or (k % 2 == 1 and x in G.neighbors(u) and (v, x) in Vk)) \
                    and ((u, v), (v, x), None) in Gk_pow_k.edges():
                        ans = k
    return ans
