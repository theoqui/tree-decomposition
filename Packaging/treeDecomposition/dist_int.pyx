"""
"""
from __future__ import print_function, absolute_import
from cysignals.memory cimport sig_malloc, sig_calloc, sig_free
from sage.graphs.base.static_sparse_graph cimport short_digraph, init_short_digraph, free_short_digraph
from sage.graphs.distances_all_pairs cimport c_distances_all_pairs
from sage.graphs.generic_graph_pyx cimport GenericGraph_pyx


cdef class Distances:
    cdef readonly GenericGraph_pyx _graph
    cdef int n
    cdef list int_to_vertex
    cdef dict vertex_to_int
    cdef short_digraph sd

    # the distance between i and j is c_distances[i * n + j]
    cdef unsigned short * c_distances

    
    def __init__(self, G):
        """
        Intialize data structure with all pair distances in G.
        """
        if not G.is_connected():
            raise ValueError("The input Graph must be connected.")

        self._graph = <GenericGraph_pyx?>G
        self.n = G.order()

        # Initialize mappings
        self.int_to_vertex = list(G)
        self.vertex_to_int = {v: i for i, v in enumerate(self.int_to_vertex)}

        init_short_digraph(self.sd, G)

        self.c_distances = c_distances_all_pairs(G, vertex_list=self.int_to_vertex)


    def __dealloc__(self):
        """
        Release memory
        """
        free_short_digraph(self.sd)
        sig_free(self.c_distances)


    def __repr__(self):
        """
        Return a string representation of ``self``.

        EXAMPLES::

            sage: G = graphs.HouseGraph()
            sage: D = Distances(G)
            sage: repr(D)
            'All pairs distances for Petersen graph'
            sage: D
            All pairs distances for Petersen graph
        """
        return "All pairs distances for {}".format(self._graph)


    def __getitem__(self, item):
        """
        Return the distance between u and v in G

        EXAMPLES::

            sage: G = graphs.HouseGraph()
            sage: D = Distances(G)
            sage: D[0, 1]
            1
        """
        return self.distance(item[0], item[1])


    def distance(self, u, v):
        """
        Return the distance between u and v in G

        EXAMPLES::

            sage: G = graphs.HouseGraph()
            sage: D = Distances(G)
            sage: D.distance(0, 1)
            1
        """
        cdef int i = self.vertex_to_int[u]
        cdef int j = self.vertex_to_int[v]
        return self.c_distances[i * self.n + j]
