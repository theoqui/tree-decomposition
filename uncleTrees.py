from sage.graphs.graph import Graph
from sage.sets.set import Set
from sage.sets.disjoint_set import DisjointSet
from itertools import combinations, chain
from decomp_tools import is_valid_tree_decomposition


def make_uncle_tree(G, t0, labels=True):
    """
    Build an uncle tree of graph G starting at vertex t0
    """
    D = set()   # "dead" vertices
    P = []      # path vertices
    B = set()   # neighbors of the path
    pk = t0
    T = dict()
    # label the vertices with intervals
    label = dict()
    label[t0] = [0, None]
    count = 0
    leaves = set()

    T[t0] = None
    while not pk == None:
        N = set(G.neighbors(pk))
        Y = N.difference(D.union(B.union(set(P))))
        if Y != set():
            x = Y.pop() # get and remove any element of Y
            T[x] = pk   # pk is the predecessor of x
            count += 1
            label[x] = [count, None]
            P.append(pk)    # add pk to the path
            pk = x
            # we add the rest of Y to B
            for v in Y:
                B.add(v)
        else:
            D.add(pk)
            P = P[:-1]
            # finalizing the label of this vertex
            label[pk] = [label[pk][0], count]
            if label[pk][0] == label[pk][1]:
                leaves.add(pk)

            old_pk = pk
            pk = T[pk]

            # choosing the vertices that need to be re-examined : N(pk) U N(pk-1)
            if pk == None:
                to_reevaluate = set(G.neighbors(old_pk))
            else:
                to_reevaluate = set(G.neighbors(old_pk) + G.neighbors(pk))
            # remove from B all the vertices that no longer have neighbors in P
            for v in to_reevaluate.difference(D).intersection(B):
                can_remove_v = True
                for w in G.neighbors(v):
                    if w != pk and w not in D and w in P:
                        can_remove_v = False
                        break
                if can_remove_v:
                    B.remove(v)
    if labels:
        return T, label, leaves
    else:
        return T


def is_ancestor(v, w, labels):
    """
    returns true if v is an ancestor of w
    i.e. if the identifier of w is in the label of v
    """
    return labels[w][0] >= labels[v][0] and labels[w][0] <= labels[v][1]


def is_uncle(v, w, T, labels):
    """
    returns true if v is an uncle of w
    i.e. if there is a child of parent(v) that is older than v and is an ancestor of w
    """
    if T[v] == None:    # if v is t0, he can't be an uncle
        return False
    parent_v = T[v]
    children_pv = [key for key, value in T.items() if value == parent_v]
    for x in children_pv:
        if labels[x][0] < labels[v][0] and is_ancestor(x, w, labels):
            return True
    return False


def is_uncle_tree(T, labels, G):
    """
    returns true if T is an uncle tree of G w.r.t. the ordering given by labels
    """

    edge_set = set(G.edges())
    # for all the edges in the graph, keep only those that aren't in the tree
    for (u, v, w) in G.edges():
        if T[u] == v or T[v] == u:
            edge_set.remove((u, v, w))

    # if we find u,v such that u is not an uncle of v and v is not an uncle of u
    # then T is not an uncle tree of G
    for (u, v, w) in edge_set:
        if not is_uncle(u, v, T, labels) and not is_uncle(v, u, T, labels):
            return False

    return True


def is_junior(s, t, labels):
    """
    returns true if s is junior to t
    labels MUST BE A DFS ORDERING
    """
    if is_ancestor(s, t, labels) or is_ancestor(t, s, labels):
        return False
    else:
        return labels[s][0] > labels[t][0]


def bag_maker(G, T, labels, k):
    """
    t: a vertex of T
    G: the original graph
    T: an uncle-tree
    k: upper bound on the size of the holes in the graph
    labels: a DFS ordering on the vertices of T
    """
    T_rev = dict()
    for v in G.vertices():
        T_rev[v] = set()

    for child, parent in T.items():
        if parent != None:
            T_rev[parent].add(child)

    def make_bag(t):
        # Xt is the bag containing all v in V(G) s.t. :
        # v in V(Qt)
        qt = set()
        n_qt = set()
        adj_qt = set()
        v = t
        while v != None and len(qt) < k-2:
            qt.add(v)
            # the vertices that touch Qt in G and are junior to t
            for w in G.neighbors(v):
                if is_junior(w, t, labels):
                    adj_qt.add(w)
            v = T[v]

        # v is a child of t in T
        child_t = set([v for v in T_rev[t]])

        return qt.union(child_t.union(adj_qt))

    return make_bag


def build_tree_decomposition(T, B):
    H = Graph(name="Uncle-tree decomposition")
    # handle the case where the only bag left is the root
    if len(T.keys()) == 1:
        H.add_vertex(Set(B[list(T)[0]]))
        return H

    for v, w in T.items():
        if v != None and w != None:
            if B[v] != B[w]:
                H.add_edge(Set(B[v]), Set(B[w]))
            else:
                print("there are identical bags")
    return H


def simplify(T, B, leaves):
    """
    T: the uncle-tree
    B: the bags of the decomposition
    leaves: the leaves of T
    Iteratively removes all the leaves whose bag is included in its parent's bag
    """
    changed = False
    new_leaves = set()  # the parents of the deleted leaves
    for v in leaves:
        pred_v = T[v]
        if pred_v != None:
            if B[pred_v].issuperset(B[v]):
                del T[v]
                new_leaves.add(pred_v)
                changed = True

    # if leaves were deleted, re-run on the new leaves, otherwise build and return the corresponding graph
    if changed:
        return simplify(T, B, new_leaves)
    else:
        return build_tree_decomposition(T, B)


def compute_k(G, T, labels):
    k = 3

    # build the tree the other way around (T_rev[v] = children(v))
    T_rev = dict()
    for v in G.vertices():
        T_rev[v] = set()
    for child, parent in T.items():
        if parent != None:
            T_rev[parent].add(child)

    for v in G.vertices():
        # compute the distance to all the vertices below v in T
        dist_below = dict()
        dist_below[v] = 0
        Q = set()
        Q.add(v)
        while not len(Q) == 0:
            u = Q.pop()
            for w in T_rev[u]:
                Q.add(w)
                dist_below[w] = dist_below[u] + 1
        # if one of the junior neighbors of G is also a junior neighbor of a vertex below v in T, we need to increase the value of k
        # for each neighbor u of v in G
        for u in G.neighbors(v):
            # if u is junior to v or u is a child of v in T
            if is_junior(u, v, labels) or u in T_rev[v]:
                # for each neighbor w of u in G that is below v in T
                for w in G.neighbors(u):
                    if w in dist_below.keys():
                        # if u is junior to w or u is a child of w in T then k is at least dist(v, w) + 2 (the hole)
                        if is_junior(u, w, labels) or u in T_rev[w]:
                            new_k = dist_below[w] + 2
                            if k < new_k:
                                k = new_k
    return k


def uncle_tree_decomposition(G, source = None, check=True):
    """
    input:  G: a graph
            check: whether to perform sanity checks and verify if the output graph is a valid decomposition
    """
    if check:
            if not G.is_connected():
                raise ValueError("G must be connected")

    # trivial case
    if len(G.vertices()) == 0:
        H = Graph()
        H.add_vertex(Set())
        return True, H

    if source == None:
        source = G.vertices()[0]

    # compute an uncle-tree of G
    T, labels, leaves = make_uncle_tree(G, source)
    # compute the minimum value of k to obtain a valid decomposition
    k = compute_k(G, T, labels)
    # get the function to build the bags of the decomposition
    make_bag = bag_maker(G, T, labels, k)
    bags = dict()

    # build one bag per vertex
    for v in G.vertices():
        bags[v] = make_bag(v)

    # simplify the decomposition to eliminate duplicate bags
    H = simplify(T, bags, leaves)

    if check and not is_valid_tree_decomposition(G, H):
        return False, H
    return True, H


def test():
    G = Graph()
    G.add_edge(0, 1)
    G.add_edge(0, 8)
    G.add_edge(1, 7)
    G.add_edge(1, 2)
    G.add_edge(7, 3)
    G.add_edge(3, 2)
    G.add_edge(2, 4)
    G.add_edge(2, 6)
    G.add_edge(6, 3)
    G.add_edge(6, 4)
    G.add_edge(4, 5)
    G.add_edge(4, 3)
    G.add_edge(5, 7)
    G.add_edge(5, 8)
    G.add_edge(8, 3)

    H = uncle_tree_decomposition(G)
    return G, H
