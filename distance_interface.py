## This ensures that the next import requests will be in the correct directory
## i.e., we get the path to this file and we move to it
import inspect,os
working_directory = os.getcwd()
source_directory = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.chdir(source_directory)
from sage.repl import load

load.load(source_directory + "/distance_interface.pyx", globals())

#from distance_interface import *

## Now that the code is loaded, we go back to the working directory
os.chdir(working_directory)
