from sage.graphs.graph import Graph
from sage.sets.set import Set
from sage.sets.disjoint_set import DisjointSet
from itertools import combinations, chain
from decomp_tools import is_valid_tree_decomposition

def lex_m_decomposition(G, check=True):
    """
    computes a clique-maximal decomposition of G
    G must be a connected graph
    sanity checks:
        - connectivity of G
    """

    if check and not G.is_connected():
        raise ValueError("The input graph is not connected")

    ordering, edges = G.lex_M(triangulation=True)
    ordering_dict = {ordering[i]:i for i in range(len(ordering))}
    G_chordal = Graph(edges)

    vertex_neighbors = dict() # for each vertex, its closed neighborhood in G_chordal
    H = Graph() # the tree-decomposition

    # a vertex v and its neighbors numbered after it form a clique, this is how we compute the cliques
    for v in ordering:
        neighborhood = G_chordal.neighbors(v, closed=True)
        if len(neighborhood) == 1:
            vertex_neighbors[v] = Set([v]), v
        else:
            if not neighborhood[0] == v:
                min_vertex = neighborhood[0]
            else:
                min_vertex = neighborhood[1]
            for u in neighborhood:
                if not u == v:
                    if ordering_dict[u] < ordering_dict[min_vertex]:
                        min_vertex = u
        vertex_neighbors[v] = Set(neighborhood), min_vertex
        H.add_vertex(Set(neighborhood))
        G_chordal.delete_vertex(v)

    for v in ordering[:-1]:
        H.add_edge(vertex_neighbors[v][0], vertex_neighbors[vertex_neighbors[v][1]][0])

    if check:
        return is_valid_tree_decomposition(G, H), H
    return True, H
