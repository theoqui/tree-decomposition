"""
Tree decompositions

This module implements tree-decomposition methods.

.. TODO:

    - Approximation of treelength based on :meth:`~sage.graphs.graph.Graph.lex_M`
"""
from sage.graphs.graph import Graph
from sage.sets.set import Set
from sage.misc.cachefunc import cached_function
from itertools import combinations, chain
from sage.sets.disjoint_set import DisjointSet
from sage.functions.other import ceil
from sage.rings.infinity import Infinity

def is_valid_tree_decomposition(G, T):
    r"""
    Check whether `T` is a valid tree-decomposition for `G`.
    """
    # 1. The union of the bags equals V
    if set(G) != set(chain(*T)):
        return False

    # 2. Each edge of G is contained in a bag
    vertex_to_bags = {u: set() for u in G}
    for Xi in T:
        for u in Xi:
            vertex_to_bags[u].add(Xi)

    for u, v in G.edge_iterator(labels=False):
        if not any(v in Xi for Xi in vertex_to_bags[u]):
            return False

    # 3. The bags containing a vertex v form a connected subset of T
    for X in vertex_to_bags.values():
        D = DisjointSet(X)
        for Xi in X:
            for Xj in T.neighbor_iterator(Xi):
                if Xj in X:
                    D.union(Xi, Xj)
        if D.number_of_subsets() > 1:
            return False

    return True


def reduced_tree_decomposition(G):
    r"""
    Return a reduced tree-decomposition of `G`.

    We merge all edges between two sets `S` and `S'` where `S` is a subset of
    `S'`. To do so, we use a simple union-find data structure to record merge
    operations and the good sets.
    """
    def get_ancestor(ancestor, u):
        if ancestor[u] == u:
            return u
        ancestor[u] = get_ancestor(ancestor, ancestor[u])
        return ancestor[u]

    ancestor = {u: u for u in G}
    for u, v in G.edge_iterator(labels=False):
        u = get_ancestor(ancestor, u)
        v = get_ancestor(ancestor, v)
        if u == v:
            continue
        elif u.issuperset(v):
            ancestor[v] = u
        elif v.issuperset(u):
            ancestor[u] = v

    H = Graph(multiedges=False, name="Reduced tree-decomposition of {}".format(G.name()))
    for u, v in G.edge_iterator(labels=False):
        u = get_ancestor(ancestor, u)
        v = get_ancestor(ancestor, v)
        if u != v:
            H.add_edge(u, v)
    return H

def nice_tree_decomposition(G, T):
    r"""
    Return a nice tree-decomposition of `G`.

    A tree-decomposition in *nice* if ...

    As several nodes may receive the same labels, we store vertex labels using
    `set_vertex`. Labels can then be retrieved using `get_vertex`.
    """
    raise NotImplementedError("to do")


def treelength_lowerbound(G):
    r"""
    Return a lower bound on the treelength of `G`.
    """
    if G.is_cycle():
        return int(ceil(G.order() / 3.0))

    lowerbound = 0
    girth = G.girth()
    if girth is not Infinity:
        lowerbound = max(lowerbound, girth / 3.0)

    return int(lowerbound)


def treelength_rec(g, k=None, certificate=False, distances=None, diameter=None):
    """
    .. NOTE::

        This method assumes that the input graph is undirected and connected.
    """
    if k is not None and k < 0:
        raise ValueError("k (= {}) must be a nonnegative integer".format(k))

    if certificate:
        name = "Tree decomposition of {}".format(g.name())

    # Trivial cases
    if g.order() <= 1:
        if certificate:
            if g:
                return Graph({Set(g): []}, format="dict_of_lists", name=name)
            return Graph(name=name)
        if k is None:
            return 0
        return True

    if distances is None:
        distances = g.distance_all_pairs()

    if diameter is None:
        diameter = max(distances[u][v] for u, v in combinations(g, 2))

    if k is not None and k >= diameter:
        if certificate:
            return Graph({Set(g): []}, format="dict_of_lists", name=name)
        return True

    # Forcing k to be defined
    if k is None:
        for i in range(treelength_lowerbound(g), diameter + 1):
            ans = treelength_rec(g, k=i, certificate=certificate,
                                 distances=distances, diameter=diameter)
            if ans:
                return ans if certificate else i


    # This is the recursion described in the method's documentation. All
    # computations are cached, and depends on the pair ``cut,
    # connected_component`` only.
    #
    # It returns either a boolean or the corresponding tree-decomposition, as a
    # list of edges between vertex cuts (as it is done for the complete
    # tree-decomposition at the end of the main function.

    def my_check(edge, nb):
        for cut in edge:
            if len(cut) > 1 and any(distances[u][v] > k for u, v in combinations(cut, 2)):
                print("problem {} with vertex {} of edge {}".format(nb, cut, edge))
            

    @cached_function
    def rec(cut, cc):

        if len(cc) == 1:
            [v] = cc
            reduced_cut = frozenset([x for x in g.neighbor_iterator(v) if x in cut])
            distances_v = distances[v]
            if any(distances_v[x] > k for x in reduced_cut):
                return False
            if certificate:
                if cut == reduced_cut:
                    return [(cut, cut.union(cc))]
                # We need to forget some vertices
                return [(cut, reduced_cut), (reduced_cut, reduced_cut.union(cc))]

            return True

        # We explore all possible extensions of the cut
        for v in cc:

            # New cuts and connected components, with v respectively added and
            # removed
            cutv = cut.union([v])
            distances_v = distances[v]
            if any(distances_v[x] > k for x in cutv):
               continue
            ccv = cc.difference([v])

            # The values returned by the recursive calls.
            sons = []

            # Removing v may have disconnected cc. We iterate on its connected
            # components
            for cci in g.subgraph(ccv).connected_components():
                cci = frozenset(cci)

                # The recursive subcalls. We remove on-the-fly the vertices from
                # the cut which play no role in separating the connected
                # component from the rest of the graph.
                reduced_cutv = frozenset([x for x in cutv
                                                 if x == v or any(xx in cci for xx in g.neighbor_iterator(x))])
                reduced_cuti = frozenset([x for x in reduced_cutv if any(xx in cci for xx in g.neighbor_iterator(x))])
                son = rec(reduced_cuti, cci)
                if not son:
                    break

                if certificate:
                    sons.append((cut, cutv))
                    if cutv != reduced_cutv:
                        sons.append((cutv, reduced_cutv))
                    if reduced_cutv != reduced_cuti:
                        sons.append((reduced_cutv, reduced_cuti))
                    sons.extend(son)

            # Weird Python syntax which is useful once in a lifetime : if break
            # was never called in the loop above, we return "sons".
            else:
                return sons if certificate else True

        return False

    # Main call to rec function, i.e. rec({v}, V-{v})
    V = list(g)
    v = frozenset([V.pop()])
    TD = rec(v, frozenset(V))

    if TD is False:
        return False

    if not certificate:
        return True

    # Building the Tree-Decomposition graph. Its vertices are cuts of the
    # decomposition, and there is an edge from a cut C1 to a cut C2 if C2 is an
    # immediate subcall of C1
    G = Graph([(Set(x), Set(y)) for x, y in TD if x != y], format="list_of_edges", name=name)
    return reduced_tree_decomposition(G)


def treelength(G, k=None, certificate=False, distances=None, algorithm=None):
    """
    Compute the tree-length of `G` (and provide a decomposition).

    INPUT:

    - ``k`` -- integer (default: ``None``); indicates the length to be
      considered. When ``k`` is an integer, the method checks that the graph has
      treelength `\leq k`. If ``k`` is ``None`` (default), the method computes
      the optimal treelength.

    - ``certificate`` -- boolean (default: ``False``); whether to return the
      tree-decomposition itself.

    - ``distances`` -- dict of dict (default: ``None``); if given, this data
      structure encodes the distance matrix of `G` is such a way that
      ``distances[u][v]`` is the distance to consider from `u` to `v` in `G`. By
      default (``None``), the shortest path distances in the unweighted graph
      are used.

    OUTPUT:

        ``g.treelength()`` returns the treelength of `g`. When `k` is specified,
        it returns ``False`` when no tree-decomposition of length `\leq k`
        exists or ``True`` otherwise. When ``certificate=True``, the
        tree-decomposition is also returned.

    ALGORITHM:

        This method virtually explores the graph of all pairs ``(vertex_cut,
        connected_component)``, where ``vertex_cut`` is a vertex cut of the
        graph of length `\leq k`, and ``connected_component`` is a
        connected component of the graph induced by `G - vertex_cut`.

        We deduce that the pair ``(vertex_cut, connected_component)`` is
        feasible with treelength `k` if ``connected_component`` is empty, or if
        a vertex ``v`` from ``vertex_cut`` can be replaced with a vertex from
        ``connected_component``, such that the pair ``(vertex_cut + v,
        connected_component - v)`` is feasible.

        In practice, this method decomposes the graph into biconnected
        components, computes the treelength of each of the components and
        returns the maximum value over all the components. When ``certificate ==
        True``, the tree-decompositions of the components are connected to each
        others by selecting a bag in each component containing cut vertex `c`
        and connecting these bags by a path.

    .. NOTE::

        The implementation would be much faster if ``cc``, the argument of the
        recursive function, was a bitset. It would also be very nice to not copy
        the graph in order to compute connected components, for this is really a
        waste of time.

    .. TODO:: 

        Use a decomposition by clique separators. Indeed, we have that `tl(G) =
        \max_{X \in T} tl(G[X])` where `T` is the set of atoms of the
        decomposition by clique separators of `G`.

        The use of the decomposition into biconnected components is an easy case
        of decomposition by clique separators.

    .. SEEALSO::

        - :meth:`~sage.graphs.graph.Graph.treewidth` computes the treewidth of a
          graph.
        - :meth:`~sage.graphs.graph_decompositions.vertex_separation.path_decomposition`
          computes the pathwidth of a graph.
        - :mod:`~sage.graphs.graph_decompositions.vertex_separation` module.

    EXAMPLES::

        sage: G = graphs.PetersenGraph()

    TESTS::

        sage: G = graphs.PetersenGraph()
    """
    if G.is_directed():
        raise ValueError("this method is defined for undirected graphs only")

    B, C = G.blocks_and_cut_vertices()

    if len(B) == 1:
        return treelength_rec(G, k=k, certificate=certificate, distances=distances)

    TD = {frozenset(b): treelength_rec(G.subgraph(b), k=k, certificate=certificate, distances=distances)
              for b in B}

    if not certificate:
        return max(TD.values())

    T = Graph([chain(*TD.values()), chain(*[g.edge_iterator() for g in TD.values()])],
              format='vertices_and_edges', name="Tree-decomposition of {}".format(G.name()))

    for c in C:
        # We search for one vertex containing c in each graph
        X = []
        for bloc, g in TD.items():
            if c not in bloc:
                continue
            for w in g:
                if c in w:
                    X.append(w)
                    break
        # We connect identified vertices by a path
        T.add_path(X)

    return T




